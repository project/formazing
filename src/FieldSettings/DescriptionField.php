<?php

namespace Drupal\formazing\FieldSettings;

use Drupal\formazing\FieldHelper\Properties\DescriptionProperties;
use Drupal\formazing\FieldViewer\Parser\DescriptionParser;

/**
 * Class DescriptionField
 *
 * @package Drupal\formazing\FieldSettings
 */
class DescriptionField extends DescriptionProperties {

  /**
   * @param $entity
   *
   * @return array
   */
  public static function generateSettings($entity) {
    $form = [];
    $form['name'] = parent::settingName($entity);
    $form['machine_name'] = parent::settingMachineName($entity);
    $form['type'] = 'description';
    $form['value'] = parent::settingValue($entity);
    $form['field_id'] = parent::settingFieldId($entity);
    $form['formazing_id'] = parent::settingFormazingId($entity);
    $form['submit'] = parent::settingSubmit();
    return $form;
  }

  /**
   * @return string
   */
  public static function getMachineTypeName() {
    return 'description';
  }

  /**
   * @return string
   */
  public static function getParser() {
    return DescriptionParser::class;
  }

}
