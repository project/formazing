<?php

namespace Drupal\formazing\FieldSettings;

use Drupal\formazing\FieldHelper\Properties\TitleProperties;
use Drupal\formazing\FieldViewer\Parser\TitleParser;

/**
 * Class TitleField
 *
 * @package Drupal\formazing\FieldSettings
 */
class TitleField extends TitleProperties {

  /**
   * @param $entity
   *
   * @return array
   */
  public static function generateSettings($entity) {
    $form = [];
    $form['name'] = parent::settingName($entity);
    $form['machine_name'] = parent::settingMachineName($entity);
    $form['type'] = 'title';
    $form['value'] = parent::settingValue($entity);
    $form['field_id'] = parent::settingFieldId($entity);
    $form['formazing_id'] = parent::settingFormazingId($entity);
    $form['submit'] = parent::settingSubmit();
    return $form;
  }

  /**
   * @return string
   */
  public static function getMachineTypeName() {
    return 'title';
  }

  /**
   * @return string
   */
  public static function getParser() {
    return TitleParser::class;
  }

}
