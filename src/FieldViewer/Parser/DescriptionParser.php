<?php

namespace Drupal\formazing\FieldViewer\Parser;

/**
 * Class DescriptionParser
 *
 * @package Drupal\formazing\FieldViewer\Parser
 */
class DescriptionParser extends Parser {

  /**
   * @param \Drupal\formazing\Entity\FieldFormazingEntity $field
   *
   * @return array
   */
  public static function parse($field) {
    $render = [
      '#type' => 'markup',
      '#markup' => '<p class="description">' . $field->getFieldValue() . '</p>',
    ];
    return $render;
  }

}
