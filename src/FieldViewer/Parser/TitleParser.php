<?php

namespace Drupal\formazing\FieldViewer\Parser;

/**
 * Class TitleParser
 *
 * @package Drupal\formazing\FieldViewer\Parser
 */
class TitleParser extends Parser {

  /**
   * @param \Drupal\formazing\Entity\FieldFormazingEntity $field
   *
   * @return array
   */
  public static function parse($field) {
    $render = [
      '#type' => 'markup',
      '#markup' => '<h3>' . $field->getFieldValue() . '</h3>',
    ];
    return $render;
  }

}
